import React from 'react';
import barrier from '../img/barrier.svg';
import './notfound.css';
import Header from '../header/header';
import { Link } from 'react-router-dom';


//o header entra como componente. ele será importado nas linhas acima e a função pronta poderá ser chamada aqui para não ter que copiar e colar o código toda hora.
export default class NotFound extends React.Component {
    render () {
    return (
        <div className="notFoundContainer container">
            <Header /> 
            <p className="notFoundNumber notFound">404</p>
            <img src={barrier} className="logoNotFound" alt="cavalete"/>
            <p className="notFoundText notFound ">Oops, essa página não existe.</p>
            < Link to="/" className="notFoundReturn">Voltar</Link>
        </div>
    );
    }
}
// /*entra aqui o header, com exatamente essa gramática
//             PS: NÃO PARECE SER O MESMO FOOTER QUE O DAS DEMAIS PASTAS*/
//a função NotFound será chamada quando o redirecionamento não levar para lugar nenhum, ou, redirecionar para esta enquanto as outras não existirem
//os parágrafos tem um nome de classe igual para colocar a mesma fonte