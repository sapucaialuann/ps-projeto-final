import React from 'react';
import './header.css';
import { Link } from 'react-router-dom';
import logoInjunior from '../img/logoInjunior.svg';
import configIcon from '../img/configIcon.svg';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    };

    Dropdown = (somaClick) => {
        if (somaClick === '1') {
            console.log('Tá indo por aqui');
            let dropDown = document.querySelector('.dropdownList');
            dropDown.classList.toggle("show");
        }
    };

    render() {
        return (
            <header className="container">
                <img className="logoHeader" src={logoInjunior} />
                <div className="dropdownHeader">
                    <button onClick={() => this.Dropdown('1')} className="dropdownBtn"><img src={configIcon} /></button>
                    <div className="dropdownList">
                        <ul>
                            <li><Link to="/inconstruction" className="dropdownItem"> Configurações</Link></li>
                            <li><Link to="/inconstruction" className="dropdownItem"> Log do sistema</Link></li>
                            <li><Link to="*" className="dropdownItem"> Log da conta</Link></li>
                            <li className="logOutBtn" ><Link to="/" className="dropdownItem"> Sair</Link></li>
                        </ul>
                    </div>
                </div>
            </header>
        );
        }
}