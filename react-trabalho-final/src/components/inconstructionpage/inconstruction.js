import React from 'react';
import barrier from '../img/barrier.svg';
import './inconstruction.css';
import Header from '../header/header';
import { Link } from 'react-router-dom';


//o header entra como componente. ele será importado nas linhas acima e a função pronta poderá ser chamada aqui para não ter que copiar e colar o código toda hora.
export default class InConstruction extends React.Component {
    render () {
    return (
        <div className="constructionContainer container">
            <Header /> 
            <img src={barrier} className="logoconstruction" alt="cavalete"/>
            <p className="constructionText construction ">Esta funcionalidade ainda não foi implementada.</p>
            < Link to="/" className="constructionReturn">Voltar</Link>
        </div>
    );
    }
}
// /*entra aqui o header, com exatamente essa gramática
//             PS: NÃO PARECE SER O MESMO FOOTER QUE O DAS DEMAIS PASTAS*/
//a função construction será chamada quando o redirecionamento não levar para lugar nenhum, ou, redirecionar para esta enquanto as outras não existirem
//os parágrafos tem um nome de classe igual para colocar a mesma fonte