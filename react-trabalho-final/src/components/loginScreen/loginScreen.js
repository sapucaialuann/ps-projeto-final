import React from 'react';
import logoInjunior from '../img/logoInjunior.svg';
import './loginScreen.css';
import { Link } from 'react-router-dom';

//as funções são como o href. Ficam dentro da tag sendo referenciadas: 
//onSubmit={this.handleSubmit}
//nos parágrafos abaixo do título é preciso colocar uma função que recebe a data ao invés de simplesmente um número por aqui.
function LeftPage() {
    return (
        <div className="loginLeftSide loginContainer">
            <img src={logoInjunior}/>
            <div className="loginParagraph">
                <h2>Processo Seletivo</h2>
                <p>Está aberto o processo seletivo de 2019.2<br />
                As inscrições irão de 01/01 até 01/01</p>
                <Link to="*" className="linkBelow">Inscreva-se</Link>
            </div>
        </div>
    )
}

function RightPage() {
    return (
        <div className="loginRightSide loginContainer">
            <h2>Área do treinamento</h2>
            <form onSubmit={this.handleSignIn}>
                <input type="email" placeholder="Endereço de e-mail" onChange={event => this.setState({ email: event.target.value })} />
                <input type="password" placeholder="Senha" onChange={event   => this.setState({ password: event.target.value })} />
                {/* <Link to="/pageColaboradores" className="submitBtn">Entrar</Link> */}
                <button type="submit" className="submitBtn">Entrar</button>
            </form>
            <Link to="/forgotpassword" className="linkBelow">Recuperar senha</Link>
            </div>
    );
}

export {LeftPage, RightPage };
