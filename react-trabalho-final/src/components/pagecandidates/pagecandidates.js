import React from 'react';
import './pagecandidates.css';
import Header from '../header/header';
import HomeAdmMenu from '../homeAdmMenu/homeAdmMenu';
import { Link } from 'react-router-dom';
import api from '../services/Api';


export default class PageCandidates extends React.Component{
    state = {users: []};

    componentDidMount() {
        api.get('/users').then(response => {
            this.setState({ users: [ ...response.data] });
        });
    };

    render() {
        const user = this.state.users.map((user) => {
            if (user.kind === 'candidate'){
                return (
                    <tr className="tableTitle">
                        <td>{user.name}</td>
                        <td>{user.kind}</td>
                        <td><Link className="editBtn" to="/" /></td>
                    </tr>
                )
            }
        });
    
        return(
            <div className="homeCollab">
                <Header />
                <div className="homeAll">
                    <div className="homeStandart">
                        <HomeAdmMenu />
                    </div>
                    <div className="homeCollabContent container">
                        <div className="collabSubHeader">
                            <h2>Candidatos</h2>
                            <input type="text" />
                        </div>
                        <table>
                            <thead>
                                <tr className="tableTitle">
                                    <th className="titleOne">Usuário</th>
                                    <th className="titleTwo">Tipo de Usuário</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {user}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    };
}


//auth/login
//json tem que ter atributo email com renatocaruso@id.uff.br
//password 123456
//atributo com chave recebida pelo json (authorization)