import React from 'react';
import logoInjunior from '../img/logoInjunior.svg';
import { Link } from 'react-router-dom';
import { LeftPage } from '../loginScreen/loginScreen';


function ForgotPassword() {
    
    return (
        <div className="loginPage container">
            <LeftPage />
            <div className="loginRightSide loginContainer">
                <h2>Recuperar senha</h2>
                <form>
                    <input type="email" placeholder="email..."/>
                    <Link to="/pageColaboradores" className="submitBtn">Enviar</Link>
                </form>
                <Link to="/" className="linkBelow">Voltar</Link>
            </div>

        </div>
    );
}
  export default ForgotPassword;
