import React from 'react';
import './homeAdmMenu.css';
import { Link } from 'react-router-dom';




export default class HomeAdmMenu extends React.Component {
    constructor(props) {
        super(props);
    };

    Accordion = (somaClick) => {
        if (somaClick === '1') {
            console.log('Tá indo por aqui');
            let accordOne = document.querySelector('#AccordionOne');
            accordOne.classList.toggle("show");
        }
        
        if (somaClick === '2') {
            console.log('Tá indo por aqui');
            let accordTwo = document.querySelector('#AccordionTwo');
            accordTwo.classList.toggle("show")
        }
    };
//this.MyFunctionDropdown.bind(this) seria isso?
    render () {
        return (
            <div className="homeAdmMenu">
                <div className="menuContent container">
                    <div className="menuDashboard">
                        <p>Dashboard</p>
                    </div>
                    <div className="menuDropdown firstMenu">
                        <button onClick={() => this.Accordion('1')} className="dropBtn">Processos seletivos</button>{/*Aqui entra a função onclick chamando o JS com o dropdown */}
                        {/* FALTA LINKAR ESSAS ANCORAS COM SEUS RESPSCTIVOS LINKS */}
                        <div className="menuDropdownItem" id="AccordionOne">
                            <Link to="/" className="dropdownItem"> Processo seletivo vigente</Link>
                            <Link to="/inconstruction" className="dropdownItem"> Todos os processos seletivos</Link>
                            <Link to="/inconstruction" className="dropdownItem"> Criar processo seletivo</Link>
                        </div>
                    </div>
                    <div className="menuDropdown secondMenu">
                    <button onClick={() => this.Accordion('2')} className="dropBtn">Usuários</button>{/*Aqui entra a função onclick chamando o JS com o dropdown */}
                        {/* FALTA LINKAR ESSAS ANCORAS COM SEUS RESPSCTIVOS LINKS */}
                        <div className="menuDropdownItem" id="AccordionTwo">
                            <Link to="/pageusers" className="dropdownItem"> Todos os usuários</Link>
                            <Link to="/pagecandidates" className="dropdownItem"> Candidatos</Link>
                            <Link to="/pageColaboradores" className="dropdownItem"> Colaboradores</Link>
                            <Link to="*" className="dropdownItem"> Criar usuário</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}