import axios from 'axios';
import {getToken} from './Auth';


const api = axios.create ({
    baseURL: 'https://api-ps.herokuapp.com'
});

api.interceptors.request.use (async config => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

export default api;








// const Token = axios.post('https://api-ps.herokuapp.com/login', {
//     'email': 'renatocaruso@id.uff.br',
//     'password': '123456'
//     }).then (response => {
//         console.log(this.data);
//         console.log(response.data);
//         console.log(response.headers);

//     })





