import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Link } from 'react-router-dom';
import logoInjunior from '../img/logoInjunior.svg';
import '../loginScreen/loginScreen.css'
import api from "./Api";
import { login } from "./Auth";
// import { LoginScreen } from "../loginScreen/loginScreen";

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    error: ""
  };

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/login", { email, password });
        login(response.data.token);
        this.props.history.push("/pagecandidates");
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais. T.T"
        });
      }
    }
  };

  render() {
    return (
      <div className ="loginPage container">
      <div className="loginLeftSide loginContainer">
          <img src={logoInjunior}/>
          <div className="loginParagraph">
              <h2>Processo Seletivo</h2>
              <p>Está aberto o processo seletivo de 2019.2<br />
              As inscrições irão de 01/01 até 01/01</p>
              <Link to="*" className="linkBelow">Inscreva-se</Link>
          </div>
      </div>
      <div className="loginRightSide loginContainer">
      <h2>Área do treinamento</h2>
      <form onSubmit={this.handleSignIn}>
          <input type="email" placeholder="Endereço de e-mail" onChange={event => this.setState({ email: event.target.value })} />
          <input type="password" placeholder="Senha" onChange={event   => this.setState({ password: event.target.value })} />
          {/* <Link to="/pageColaboradores" className="submitBtn">Entrar</Link> */}
          <button type="submit" className="submitBtn">Entrar</button>
      </form>
      <Link to="/forgotpassword" className="linkBelow">Recuperar senha</Link>
      </div>
      </div> 

      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      //<LoginScreen />
      // <div>
      //   <form onSubmit={this.handleSignIn}>
      //     {this.state.error && <p>{this.state.error}</p>}
      //     <input
      //       type="email"
      //       placeholder="Endereço de e-mail"
      //       onChange={e => this.setState({ email: e.target.value })}
      //     />
      //     <input
      //       type="password"
      //       placeholder="Senha"
      //       onChange={e => this.setState({ password: e.target.value })}
      //     />
      //     <button type="submit">Entrar</button>
      //     <hr />
      //     <Link to="*">Criar conta grátis</Link>
      //   </form>
      // </div>
    );
  }
}

export default withRouter(SignIn);