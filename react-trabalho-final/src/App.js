import React from 'react';
import NotFound from './components/notfound/notfound';
import HomeAdmMenu from './components/homeAdmMenu/homeAdmMenu';
import LoginScreen from './components/loginScreen/loginScreen';
import Header from './components/header/header';


function App() {
  return <div><Header /><NotFound /></div>
  // <Header /> essa função vem dentro das outras páginas, ou pode estar referenciada aqui...
  // <LoginScreen /> Essa função é a primeira ser chamada. É ela que vai fazer as demais funções.
  // <HomeAdmMenu /> função vai ser chamada dentro de cada função, de acordo com co tipo do usuário
  // <NotFound />  foi retirada para chegar o home-adm

}
//transformar em classe e colocar o appdidmount
export default App;
