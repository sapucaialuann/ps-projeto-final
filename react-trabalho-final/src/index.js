import React from 'react';
import ReactDOM from 'react-dom';
import './components/css/reset.css';
import './components/css/general.css';
import './index.css';
import NotFound from './components/notfound/notfound';
import InConstruction from './components/inconstructionpage/inconstruction';
import PageUsers from './components/pageusuarios/pageusers';
import PageCandidates from './components/pagecandidates/pagecandidates';
// import { LoginScreen } from './components/loginScreen/loginScreen';
import SignIn from './components/services/signIn';
import ForgotPassword from './components/forgotpassword/forgotpassword';
import PageColaboradores from './components/pagecolaboradores/pagecolaboradores';
import { BrowserRouter, Switch, Route} from 'react-router-dom'; // para gerar as routes
import * as serviceWorker from './serviceWorker';
import { withRouter } from "react-router-dom";


ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={SignIn} />
            <Route exact path="/pageColaboradores" component={PageColaboradores} />
            <Route exact path="/forgotpassword" component={ForgotPassword} />
            <Route exact path="/inconstruction" component={InConstruction} />
            <Route exact path="/pageusers" component={PageUsers} />
            <Route exact path="/pagecandidates" component={PageCandidates} />
            <Route exact path="*" component={NotFound} />
        </Switch>
    </BrowserRouter>
, document.getElementById('root'));

// /* <Route path="/app" component={App} /> */
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
